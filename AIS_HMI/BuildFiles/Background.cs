//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Neo.ApplicationFramework.Generated {
    using Neo.ApplicationFramework.Controls.Controls;
    using Neo.ApplicationFramework.Controls;
    using Neo.ApplicationFramework.Interfaces;
    using Neo.ApplicationFramework.Tools.Security;
    using Neo.ApplicationFramework.Tools.Actions;
    using Neo.ApplicationFramework.Common.MultiLanguage;
    
    
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
    public partial class Background : Neo.ApplicationFramework.Controls.Controls.Form, Neo.ApplicationFramework.Interfaces.ISupportMultiLanguage {
        
        private Neo.ApplicationFramework.Controls.Controls.Label m_Text;
        
        private Neo.ApplicationFramework.Controls.Controls.DigitalClockCF m_DigitalClock;
        
        private Neo.ApplicationFramework.Controls.Controls.DigitalClockCF m_DigitalClock1;
        
        private bool m_Initialized_Background;
        
        public Background() {
            this.InitializeComponent();
            this.ApplyLanguageInitialize();
        }
        
        protected Neo.ApplicationFramework.Controls.Script.TextControlCFAdapter Text {
            get {
                return this.AdapterService.CreateAdapter<Neo.ApplicationFramework.Controls.Script.TextControlCFAdapter>(this.m_Text);
            }
        }
        
        protected Neo.ApplicationFramework.Controls.Script.TextControlCFAdapter DigitalClock {
            get {
                return this.AdapterService.CreateAdapter<Neo.ApplicationFramework.Controls.Script.TextControlCFAdapter>(this.m_DigitalClock);
            }
        }
        
        protected Neo.ApplicationFramework.Controls.Script.TextControlCFAdapter DigitalClock1 {
            get {
                return this.AdapterService.CreateAdapter<Neo.ApplicationFramework.Controls.Script.TextControlCFAdapter>(this.m_DigitalClock1);
            }
        }
        
        private void InitializeComponent() {
            this.m_Text = new Neo.ApplicationFramework.Controls.Controls.Label();
            this.m_DigitalClock = new Neo.ApplicationFramework.Controls.Controls.DigitalClockCF();
            this.m_DigitalClock1 = new Neo.ApplicationFramework.Controls.Controls.DigitalClockCF();
            ((System.ComponentModel.ISupportInitialize)(this.m_Text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_DigitalClock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_DigitalClock1)).BeginInit();
            this.SuspendLayout();
            // 
            // Background
            // 
            this.Background = new Neo.ApplicationFramework.Common.Graphics.Logic.BrushCF(System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0))))), System.Drawing.Color.Empty, Neo.ApplicationFramework.Interfaces.FillDirection.None);
            this.BorderStyle = Neo.ApplicationFramework.Interfaces.ScreenBorderStyle.ThreeDBorder;
            this.ControlBox = false;
            // 
            // m_Text
            // 
            this.m_Text.BlinkDynamicsValue = false;
            this.m_Text.DelayMouseInputPeriod = 0;
            this.m_Text.EnabledDynamicsValue = true;
            this.m_Text.FontFamily = "Segoe UI";
            this.m_Text.FontSizePixels = 20;
            this.m_Text.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(134)))), ((int)(((byte)(202)))), ((int)(((byte)(221)))));
            this.m_Text.Height = 21;
            this.m_Text.Left = 20;
            this.m_Text.Name = "m_Text";
            this.m_Text.Padding = new Neo.ApplicationFramework.Common.Graphics.Logic.ThicknessCF(5D, 0D, 5D, 1D);
            this.m_Text.ScreenOwnerName = "Background";
            this.m_Text.Top = 20;
            this.m_Text.VisibleDynamicsValue = true;
            this.m_Text.Width = 46;
            // 
            // m_DigitalClock
            // 
            this.m_DigitalClock.BlinkDynamicsValue = false;
            this.m_DigitalClock.DelayMouseInputPeriod = 0;
            this.m_DigitalClock.DisplayFormat = Neo.ApplicationFramework.Interfaces.DateTimeDisplayFormat.Time;
            this.m_DigitalClock.EnabledDynamicsValue = true;
            this.m_DigitalClock.FontFamily = "Segoe UI";
            this.m_DigitalClock.FontSizePixels = 20;
            this.m_DigitalClock.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(134)))), ((int)(((byte)(202)))), ((int)(((byte)(221)))));
            this.m_DigitalClock.Height = 27;
            this.m_DigitalClock.Left = -47;
            this.m_DigitalClock.Name = "m_DigitalClock";
            this.m_DigitalClock.ScreenOwnerName = "Background";
            this.m_DigitalClock.ShowDayOfWeek = false;
            this.m_DigitalClock.ShowSeconds = false;
            this.m_DigitalClock.Text = "12:40 PM";
            this.m_DigitalClock.Top = 436;
            this.m_DigitalClock.VisibleDynamicsValue = true;
            this.m_DigitalClock.Width = 207;
            // 
            // m_DigitalClock1
            // 
            this.m_DigitalClock1.BlinkDynamicsValue = false;
            this.m_DigitalClock1.DelayMouseInputPeriod = 0;
            this.m_DigitalClock1.DisplayFormat = Neo.ApplicationFramework.Interfaces.DateTimeDisplayFormat.Date;
            this.m_DigitalClock1.EnabledDynamicsValue = true;
            this.m_DigitalClock1.FontFamily = "Segoe UI";
            this.m_DigitalClock1.FontSizePixels = 20;
            this.m_DigitalClock1.Foreground = System.Drawing.Color.FromArgb(((int)(((byte)(134)))), ((int)(((byte)(202)))), ((int)(((byte)(221)))));
            this.m_DigitalClock1.Height = 27;
            this.m_DigitalClock1.Left = 599;
            this.m_DigitalClock1.Name = "m_DigitalClock1";
            this.m_DigitalClock1.ScreenOwnerName = "Background";
            this.m_DigitalClock1.ShowDayOfWeek = false;
            this.m_DigitalClock1.ShowSeconds = false;
            this.m_DigitalClock1.Text = "4/21/2020";
            this.m_DigitalClock1.Top = 436;
            this.m_DigitalClock1.VisibleDynamicsValue = true;
            this.m_DigitalClock1.Width = 265;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.ClientSize = new System.Drawing.Size(800, 480);
            this.IsCacheable = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.ModalScreen = true;
            this.Name = "Background";
            this.PopupScreen = false;
            this.ScreenID = null;
            this.ScreenTitle = "";
            this.StyleName = "";
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.m_Initialized_Background = true;
            this.AddControlsAndPrimitives();
            this.ResumeLayout(false);
        }
        
        protected override Neo.ApplicationFramework.Common.Alias.Entities.AliasInstancesCF CreateInstanceData() {
            System.Collections.Generic.List<Neo.ApplicationFramework.Common.Alias.Entities.AliasInstanceCF> instanceList = new System.Collections.Generic.List<Neo.ApplicationFramework.Common.Alias.Entities.AliasInstanceCF>(1);
            instanceList.Add(this.CreateInstanceData_Default());
            Neo.ApplicationFramework.Common.Alias.Entities.AliasInstancesCF aliasInstances = new Neo.ApplicationFramework.Common.Alias.Entities.AliasInstancesCF();
            aliasInstances.Instances = instanceList.ToArray();
            return aliasInstances;
        }
        
        private Neo.ApplicationFramework.Common.Alias.Entities.AliasInstanceCF CreateInstanceData_Default() {
            Neo.ApplicationFramework.Common.Alias.Entities.AliasInstanceCF aliasinstancecf1 = new Neo.ApplicationFramework.Common.Alias.Entities.AliasInstanceCF();
            aliasinstancecf1.Name = "Default";
            aliasinstancecf1.Values = new Neo.ApplicationFramework.Common.Alias.Entities.AliasValueCF[0];
            return aliasinstancecf1;
        }
        
        protected override void BindAliases() {
        }
        
        protected override void Dispose(bool disposing) {
            base.Dispose(disposing);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public override void AddControlsAndPrimitives() {
            if (!m_Initialized_Background) {
                return;
            }
            this.AddControls();
            this.AddDrawingPrimitives();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public override void AddControls() {
            base.AddControls();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public override void AddDrawingPrimitives() {
            base.AddDrawingPrimitives();
            this.DrawingPrimitives.Add(this.m_Text);
            this.DrawingPrimitives.Add(this.m_DigitalClock);
            this.DrawingPrimitives.Add(this.m_DigitalClock1);
            ((System.ComponentModel.ISupportInitialize)(this.m_Text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_DigitalClock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_DigitalClock1)).EndInit();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        private void ApplyLanguageInternal() {
            Neo.ApplicationFramework.Tools.MultiLanguage.MultiLanguageResourceManager resources = new Neo.ApplicationFramework.Tools.MultiLanguage.MultiLanguageResourceManager(typeof(Background));
            this.m_Text.Text = resources.GetText("Background.Text.Text", "HMI 8.2");
            this.ApplyResourcesOnForm();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        void Neo.ApplicationFramework.Interfaces.ISupportMultiLanguage.ApplyLanguage() {
            this.ApplyLanguage();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        protected override void ApplyLanguage() {
            if (((Neo.ApplicationFramework.Interfaces.IScreen)(this)).IsCachedDeactivated) {
                return;
            }
            this.ApplyLanguageInternal();
            base.ApplyLanguage();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public override void ApplyLanguageInitialize() {
            if (!m_Initialized_Background) {
                return;
            }
            base.ApplyLanguageInitialize();
            this.ApplyLanguageInternal();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public override void ConnectDataBindings() {
            base.ConnectDataBindings();
        }
    }
}
