using System.Collections;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Reflection;
using System.IO;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Threading;
using Core.Api.Application;
using Core.Api.Service;
using Core.Api.Tools;
using Core.Api.Utilities;
using Core.Utilities.Utilities;
using Core.Engine.Application;
using Neo.ApplicationFramework.Attributes;
using Neo.ApplicationFramework.Common;
using Neo.ApplicationFramework.Common.Runtime;
using Neo.ApplicationFramework.Common.TypeConverters;
using Neo.ApplicationFramework.Common.Utilities;
using Neo.ApplicationFramework.Interfaces;
using Neo.ApplicationFramework.Measurement;
using Neo.ApplicationFramework.Storage.Settings;
using Neo.ApplicationFramework.Tools.Runtime;
using Neo.ApplicationFramework.Tools.Storage;

[assembly: AssemblyVersion("2.44.53.0")]
[assembly: NeoDesignerVersion("2.44.53.0")]

namespace Neo.ApplicationFramework.Generated
{
    public class Globals : GlobalsBase
    {
        private static readonly log4net.ILog m_Log = log4net.LogManager.GetLogger(typeof(Globals));

        static Globals()
        {
            
        }

        public Globals() 
            : base(CreateToolManager())
        {
            m_ProjectSettings.MainScreenTitle = "AIS_HMI";
            m_ProjectSettings.Topmost = false;
            m_ProjectSettings.StartupLocation = new Point(0, 0);
            m_ProjectSettings.MaximizeOnStartup = false;
            m_ProjectSettings.UseWideScrollbars = false;
            m_ProjectSettings.MainScreenSize = new Size(800,480);
            m_ProjectSettings.BorderStyle = ScreenBorderStyle.ThreeDBorder;
            m_ProjectSettings.InputDelay = 2000;
            m_ProjectSettings.IsOnScreenKeyboardEnabled = true;
            m_ProjectSettings.KeyboardLayoutName = "US";
            m_ProjectSettings.TerminalGroup = TerminalGroup.Default;
            List<IStorageProviderSetting> storageProviderSettingsList = new List<IStorageProviderSetting>() {
                new ProjectStorageProviderSetting("BackupAtStartup", false), new ProjectStorageProviderSetting("MaxSize", 0), 
            };
            m_ProjectSettings.StorageProviderSettings = new LocallyHostedProjectStorageProviderSettings("SQLite Database", storageProviderSettingsList, "");
            m_ProjectSettings.EnableWatchDogSettings = false;
            m_ProjectSettings.WatchDogTimeOut = 120;
            m_SystemSettings.AutomaticallyTurnOfBacklight = false;
            m_SystemSettings.BacklightTimeout = 900;    
            m_SystemSettings.KeepBacklightOnIfNotifierWindowIsVisible = false;
            Dictionary<ComPort, PortMode> comPortModes = new Dictionary<ComPort, PortMode>();
            comPortModes.Add(ComPort.COM1, PortMode.Nonconfigurable); comPortModes.Add(ComPort.COM2, PortMode.rs422); comPortModes.Add(ComPort.COM3, PortMode.Nonconfigurable); comPortModes.Add(ComPort.COM4, PortMode.rs485);                            
            m_SystemSettings.ComPortModes = comPortModes;                            
            m_SystemSettings.KeyBeep = true;
            m_SystemSettings.TimeZoneDisplayName = "";
            m_SystemSettings.TimeZoneId = -1;
            m_SystemSettings.RegionLCID = 0;
            m_SystemSettings.AdjustForDaylightSaving = true;
            m_SystemSettings.FtpServerEnabled = false;
            m_SystemSettings.FtpServerFriendlyNamesEnabled = false;
            m_SystemSettings.FtpServerAllowAnonymous = false;
            m_SystemSettings.FtpServerSdCardAccess = false;
            m_SystemSettings.FtpServerUsbAccess = false;
            m_SystemSettings.FtpServerDefaultDir = @"";
            m_SystemSettings.NTLMUser = @"";
            m_SystemSettings.NTLMPassword = @"";
            m_SystemSettings.VncServerEnabled = false;
            m_SystemSettings.VncTcpPort = 5900;
            m_SystemSettings.VncHttpTcpPort = 5800;
            m_SystemSettings.VncViewOnlyPassword = @"";
            m_SystemSettings.VncFullAccessPassword = @"";
            m_SystemSettings.VncActiveConnectionNotification = false;
            m_SystemSettings.IsKeyPanel = false;
            m_SystemSettings.IsHeadless = false;
            m_SystemSettings.AlarmButtonShowScreenTarget = @"";
            m_SystemSettings.ScreenRotationAngle = 0;
            m_SystemSettings.BeShellMenuPassword = @"";
            m_SystemSettings.ProjectGuid = new Guid("4d743950-0083-410a-8965-713eeb3c8616");
        }
        
        
        private static IToolManager CreateToolManager()
        {
            string executablePath = typeof(Globals).Module.FullyQualifiedName;            
            // take simulation into account
            var coreApplication = Environment.OSVersion.Platform != PlatformID.WinCE ? (ICoreApplication)new CoreApplication(true, executablePath) : (ICoreApplication)new CoreApplicationCe();
            IToolManager toolManager = new ApplicationEngineCe().CreateToolManager(true, coreApplication, Path.Combine(coreApplication.StartupPath, "Modules.cfgtool"));
            return toolManager;
        }
            

        
       

        /// <summary>
        /// Neo generated code - do not modify
        /// the contents of this method(s) with the code editor.
        /// </summary>        
                    public static IPrinterDevice Printer1
                    {
                        get
                        {
                            IDeviceManagerServiceCF deviceManagerService = ServiceContainerCF.GetService<IDeviceManagerServiceCF>();
                            return deviceManagerService.GetOutputDevice<IPrinterDevice>();
                        }
                    }           
                    public static IScreenAdapter NewAisScreen
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<NewAisScreen>("Screens.NewAisScreen.Default").Adapter;
                        }
                    }           
                    public static IScreenAdapter RecentEvents
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<RecentEvents>("Screens.RecentEvents.Default").Adapter;
                        }
                    }           
                    public static IScreenAdapter StatusSummary
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<StatusSummary>("Screens.StatusSummary.Default").Adapter;
                        }
                    }           
                    public static IScreenAdapter Restarting
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<Restarting>("Screens.Restarting.Default").Adapter;
                        }
                    }           
                    public static IScreenAdapter ShutdownCompleted
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<ShutdownCompleted>("Screens.ShutdownCompleted.Default").Adapter;
                        }
                    }           
                    public static IScreenAdapter RemoveKey
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<RemoveKey>("Screens.RemoveKey.Default").Adapter;
                        }
                    }           
                    public static IScreenAdapter ShutdownPending
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<ShutdownPending>("Screens.ShutdownPending.Default").Adapter;
                        }
                    }           
                    public static IScreenAdapter ShutdownWhen
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<ShutdownWhen>("Screens.ShutdownWhen.Default").Adapter;
                        }
                    }           
                    public static IScreenAdapter ShutdownYesOrNo
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<ShutdownYesOrNo>("Screens.ShutdownYesOrNo.Default").Adapter;
                        }
                    }           
                    public static IScreenAdapter AutomaticShutdown
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<AutomaticShutdown>("Screens.AutomaticShutdown.Default").Adapter;
                        }
                    }           
                    public static IScreenAdapter NoComputerResponse
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<NoComputerResponse>("Screens.NoComputerResponse.Default").Adapter;
                        }
                    }           
                    public static IScreenAdapter StartingAIS
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<StartingAIS>("Screens.StartingAIS.Default").Adapter;
                        }
                    }           
                    public static IScreenAdapter CoveyorTest
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<CoveyorTest>("Screens.CoveyorTest.Default").Adapter;
                        }
                    }           
                    public static ScriptModule1 ScriptModule1
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<ScriptModule1>("ScriptModule1");
                        }
                    }           
                    public static AlarmServer AlarmServer
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<AlarmServer>("AlarmServer");
                        }
                    }           
                    public static Tags Tags
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<Tags>("Tags");
                        }
                    }           
                    public static MultipleLanguages MultipleLanguages
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<MultipleLanguages>("MultipleLanguages");
                        }
                    }           
                    public static Expressions Expressions
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<Expressions>("Expressions");
                        }
                    }           
                    public static LicenseRootComponent LicenseRootComponent
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<LicenseRootComponent>("LicenseRootComponent");
                        }
                    }           
                    public static Security Security
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<Security>("Security");
                        }
                    }           
                    public static ProjectConfiguration ProjectConfiguration
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<ProjectConfiguration>("ProjectConfiguration");
                        }
                    }           
                    public static SntpClientRootComponent SntpClientRootComponent
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<SntpClientRootComponent>("SntpClientRootComponent");
                        }
                    }           
                    public static IScreenAdapter Background
                    {
                        get
                        {
                            return GlobalReferenceService.Value.GetObject<Background>("Screens.Background.Default").Adapter;
                        }
                    }   
        [MTAThread]
        static void Main(string[] args)
        {
            Thread.CurrentThread.Name = "NeoMainThread";
            
            InitializeBeHwApiLog();
            
            
            
            UserStartupMessage.SendUserStartupMessageToBeijerShell("Loading Files");
            if (!StopWatchCF.Silent)
                StopWatchCF.Send("Starting Project");
            StopWatchCF.SetTimestamp("***** Project Startup Time *****");
            while (ProcessExplorer.WaitForAttachDebugger)
            {
                Thread.Sleep(1000);
            }
            IsCompiledForCE = true;
            IsCompiledForDesktopWindowsPanel = false;

            Instance = new Globals();
        	if (!Instance.CheckIfApplicationCanRun())
				return;

            string executingAssemblyName = Assembly.GetExecutingAssembly().FullName;
            string executablePath = typeof(Globals).Module.FullyQualifiedName;
            Instance.Go(executingAssemblyName, executablePath, args, new string[]{"SntpClientRootComponent","ProjectConfiguration","Security","LicenseRootComponent","Expressions","Tags","MultipleLanguages","AlarmServer","Controller1","ScriptModule1","ProjectImages"}, new string[]{}, () => CoveyorTest);
        }

    }
}