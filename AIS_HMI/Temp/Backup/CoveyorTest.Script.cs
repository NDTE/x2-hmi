//--------------------------------------------------------------
// Press F1 to get help about using script.
// To access an object that is not located in the current class, start the call with Globals.
// When using events and timers be cautious not to generate memoryleaks,
// please see the help for more information.
//---------------------------------------------------------------

namespace Neo.ApplicationFramework.Generated
{
    using System.Windows.Forms;
    using System;
    using System.Drawing;
    using Neo.ApplicationFramework.Tools;
    using Neo.ApplicationFramework.Common.Graphics.Logic;
	using Neo.ApplicationFramework.Controls.Script;
    using Neo.ApplicationFramework.Controls;
    using Neo.ApplicationFramework.Interfaces;
	using System.Threading;
	using System.Collections.Generic;
    
    
	public partial class CoveyorTest
	{
		BrushCF cBlack = Color.Black;
		BrushCF cDarkGray = Color.DarkGray;
		BrushCF cLightGray = Color.LightGray;
		BrushCF cYellow = Color.Yellow;
		BrushCF cGreen = Color.Green;
		BrushCF cRed = Color.Red;
		BrushCF cOrange = Color.Orange;
		BrushCF cBlue = Color.Blue;
		BrushCF cPurple = Color.Purple;
	
		private int rect1Position = 110;
		private int rect2Position = 180;
		private int rect3Position = 250;
		private int rect4Position = 320;
		private int rect5Position = 390;
		private int rect6Position = 460;
		private int rect7Position = 530;
		private int rect8Position = 600;
		
		public Dictionary<int,ShapeCFAdapter> positions = new Dictionary<int,ShapeCFAdapter>();
		
		void Animate_Conveyor() 
		{
			for(int i = 0; i < 70; i++)
			{
				this.Rectangle.Left += 1;
				
				this.Rectangle1.Left += 1;
				this.Rectangle2.Left += 1;
				this.Rectangle3.Left += 1;
				this.Rectangle4.Left += 1;
				this.Rectangle5.Left += 1;
				this.Rectangle6.Left += 1;
				this.Rectangle7.Left += 1;
				this.Rectangle8.Left += 1;
				Update();
				Thread.Sleep(13);
			}
			color_onstation();
			
			this.Rectangle.Left = 40;
			
			this.Rectangle1.Left = rect1Position;
			this.Rectangle2.Left = rect2Position;
			this.Rectangle3.Left = rect3Position;
			this.Rectangle4.Left = rect4Position;
			this.Rectangle5.Left = rect5Position;
			this.Rectangle6.Left = rect6Position;
			this.Rectangle7.Left = rect7Position;
			this.Rectangle8.Left = rect8Position;
			
		}
		
		void Button1_Click(System.Object sender, System.EventArgs e)
		{
			Animate_Conveyor();
		}
		
		void color_onstation()
		{
			BrushCF R1Color = this.Rectangle1.Fill;
			BrushCF R2Color = this.Rectangle2.Fill;
			BrushCF R3Color = this.Rectangle3.Fill;
			BrushCF R4Color = this.Rectangle4.Fill;
			BrushCF R5Color = this.Rectangle5.Fill;
			BrushCF R6Color = this.Rectangle6.Fill;
			BrushCF R7Color = this.Rectangle7.Fill;
			BrushCF R8Color = this.Rectangle8.Fill;
			
			BrushCF R1Outline = this.Rectangle1.Outline;
			BrushCF R2Outline = this.Rectangle2.Outline;
			BrushCF R3Outline = this.Rectangle3.Outline;
			BrushCF R4Outline = this.Rectangle4.Outline;
			BrushCF R5Outline = this.Rectangle5.Outline;
			BrushCF R6Outline = this.Rectangle6.Outline;
			BrushCF R7Outline = this.Rectangle7.Outline;
			BrushCF R8Outline = this.Rectangle8.Outline;
			
			this.Rectangle1.Fill = this.Rectangle.Fill;
			this.Rectangle2.Fill = R1Color;
			this.Rectangle3.Fill = R2Color;
			this.Rectangle4.Fill = R3Color;
			this.Rectangle5.Fill = R4Color;
			this.Rectangle6.Fill = R5Color;
			this.Rectangle7.Fill = R6Color;
			this.Rectangle8.Fill = R7Color;
			
			this.Rectangle1.Outline = this.Rectangle.Outline;
			this.Rectangle2.Outline = R1Outline;
			this.Rectangle3.Outline = R2Outline;
			this.Rectangle4.Outline = R3Outline;
			this.Rectangle5.Outline = R4Outline;
			this.Rectangle6.Outline = R5Outline;
			this.Rectangle7.Outline = R6Outline;
			this.Rectangle8.Outline = R7Outline;
		}
		
		void Set_Position_Color(int position, string fillColor, string outlineColor) 
		{
			BrushCF colorForFill = cBlack;
			BrushCF colorForOutline = cLightGray;
			
			//Identify the color to fill the rectangle
			switch(fillColor.ToLower()) 
			{
				case "black":
					colorForFill = cBlack;
					break;
				case "darkgray":
					colorForFill = cDarkGray;
					break;
				case "lightgray":
					colorForFill = cLightGray;
					break;
				case "yellow":
					colorForFill = cYellow;
					break;
				case "green":
					colorForFill = cGreen;
					break;
				case "red":
					colorForFill = cRed;
					break;
				case "orange":
					colorForFill = cOrange;
					break;
				case "blue":
					colorForFill = cBlue;
					break;
				case "purple":
					colorForFill = cPurple;
					break;
				default:
					break;
			}
			
			//Identify the color to make the border of the rectangle
			switch(outlineColor.ToLower()) 
			{
				case "black":
					colorForOutline = cBlack;
					break;
				case "darkgray":
					colorForOutline = cDarkGray;
					break;
				case "lightgray":
					colorForOutline = cLightGray;
					break;
				case "yellow":
					colorForOutline = cYellow;
					break;
				case "green":
					colorForOutline = cGreen;
					break;
				case "red":
					colorForOutline = cRed;
					break;
				case "orange":
					colorForOutline = cOrange;
					break;
				case "blue":
					colorForOutline = cBlue;
					break;
				case "purple":
					colorForOutline = cPurple;
					break;
				default:
					break;
			}
			positions[position].Fill = colorForFill;
			positions[position].Outline = colorForOutline;
		}
		
		void CoveyorTest_Opened(System.Object sender, System.EventArgs e)
		{
			positions.Add(1,this.Rectangle1);
			positions.Add(2,this.Rectangle2);
			positions.Add(3,this.Rectangle3);
			positions.Add(4,this.Rectangle4);
			positions.Add(5,this.Rectangle5);
			positions.Add(6,this.Rectangle6);
			positions.Add(7,this.Rectangle7);
			positions.Add(8,this.Rectangle8);
		}
		

		
		void Button3_Click(System.Object sender, System.EventArgs e)
		{
			
			Set_Position_Color((int)this.AnalogNumeric.Value, "Red", "LightGray");
			Update();
		}
		
		void Button4_Click(System.Object sender, System.EventArgs e)
		{
			Set_Position_Color((int)this.AnalogNumeric.Value, "Yellow", "LightGray");
			Update();
		}
		
		void Button5_Click(System.Object sender, System.EventArgs e)
		{
			Set_Position_Color((int)this.AnalogNumeric.Value, "Green", "LightGray");
			Update();
		}
		
		void Button6_Click(System.Object sender, System.EventArgs e)
		{
			Set_Position_Color((int)this.AnalogNumeric.Value, "DarkGray", "LightGray");
			Update();
		}
		
		void Button7_Click(System.Object sender, System.EventArgs e)
		{
			
			Set_Position_Color((int)this.AnalogNumeric.Value, "Black", "DarkGray");
			Update();
		}
    }
}
